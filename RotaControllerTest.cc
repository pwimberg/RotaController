/*
Library to access a PI GCS controller
tested for a PI C-863 Mercurz DC Motor Controller connected to a rotational stage
written by Peter Wimberger (summer student, online: @topsrek, always avilable for questions) 
and Stefano Mersi (supervisor) in Summer 2018
tested for the XDAQ Supervisor for CMS telescope CHROMIE at CERN
*/

#include "RotaController.h"
#include <iostream>
#include <cstdlib>

int main(int argc, char const *argv[])
{
    const char *devname = "/dev/RotaController"; //devname of connected Physik Instrumente controller
    int baudrate = 38400;                        //must be configured according to the DIP-Switches on the device (5=OFF, 6=ON corresponds to 38400 Hz)
    const char *stagetype = "M-062.DG";          //will be looked up in the Stage Database locally on the PC (/usr/local/PI/pi_gcs_translator)
    const char *axisname = "1";                  //axis name (can be determined with .checklistAllAxes or qSAI) to be passed with every command (there are controllers capable of handling multiple axes connected at once)

    double target_position = 0; //move to this position after connecting, configuring and referencing.

    RotaController rcont;
    if (!rcont.connect(devname, baudrate)) 
        return -1; //return -1 and quit program when the call to the hardware is not successful 
                   //(same procedure for every hardware changing function)
    //std::cout<<rcont.getID()<<std::endl; //gets internal ID (is also logged while connecting)
    if (!rcont.checkIDN()) //get ID and log it (default: to std::out)
        return -1;

    std::cout << rcont.getParameterAddresses() << std::endl; //prints out all command information (omitting speeds up connecting tremendously)

    rcont.checkListAllAxes(); //Logs List of currently connected axes

    if (!rcont.initializeStage(axisname, stagetype)) //assigns the stagetype to the specified axis (must exist)
        return -1;

    //Parameter 22 (0x16): Value at Reference Position (Phys. Unit)
    const unsigned int *ref_val_parameteraddress = new unsigned int(22);
    const double *ref_val_parametervalue = new double(1.4);
    const char ref_val_szStrings[512] = ""; //szStrings is needed for parameters, which require chars (for example stage name: 0x3C)
    rcont.setParameter(axisname, ref_val_parameteraddress, ref_val_parametervalue, ref_val_szStrings);
    std::cout << "Set address: " << *ref_val_parameteraddress << "\tvalue: " << *ref_val_parametervalue << std::endl;

    //Parameter 21 (0x15): Maximum Travel In Positive Direction (Phys. Unit)
    const unsigned int *max_pos_parameteraddress = new unsigned int(21);
    const double *max_pos_parametervalue = new double(4);
    const char max_pos_szStrings[512] = ""; //szStrings is needed for parameters, which require chars (for example stage name: 0x3C)
    rcont.setParameter(axisname, max_pos_parameteraddress, max_pos_parametervalue, max_pos_szStrings);
    std::cout << "Set address: " << *max_pos_parameteraddress << "\tvalue: " << *max_pos_parametervalue << std::endl;

    //Parameter 48 (0x30): Maximum Travel In Negative Direction (Phys. Unit)
    const unsigned int *min_pos_parameteraddress = new unsigned int(48);
    const double *min_pos_parametervalue = new double(4);
    const char min_pos_szStrings[512] = ""; //szStrings is needed for parameters, which require chars (for example stage name: 0x3C)
    rcont.setParameter(axisname, min_pos_parameteraddress, min_pos_parametervalue, min_pos_szStrings);
    std::cout << "Set address: " << *min_pos_parameteraddress << "\tvalue: " << *min_pos_parametervalue << std::endl;

    //Parameter 63 (0x3F): On-Target-Status Settling Time
    const unsigned int *parameteraddress = new unsigned int(63);
    const double *parametervalue = new double(0.0001); //a non-zero value is needed to measure the achieved position accurately after moving
    const char szStrings[512] = "";                    //szStrings is needed for parameters, which require chars (for example stage name: 0x3C)
    rcont.setParameter(axisname, parameteraddress, parametervalue, szStrings);
    std::cout << "Set address: " << *parameteraddress << "\tvalue: " << *parametervalue << std::endl;

    unsigned int *get_parameteraddress = new unsigned int(63);
    double *get_parametervalue = new double(0);
    char get_szStrings[2048] = "";                                                               //szStrings is needed for parameters, which require chars (for example stage name: 0x3C)
    rcont.getParameter(axisname, get_parameteraddress, get_parametervalue, get_szStrings, 2048); //2048 is the maxNameSize for the string
    std::cout << "Got address: " << *get_parameteraddress << "\tvalue: " << *get_parametervalue << "\tstring: " << get_szStrings << std::endl;

    //Parameter 73 (0x49): Closed-Loop Velocity (means normal velocity)
    const unsigned int *parameteraddress2 = new unsigned int(73);
    const double *parametervalue2 = new double(5); //phys. unit/s
    const char *szStrings2 = new char[512];        //szStrings is needed for parameters, which require chars (for example stage name: 0x3C)
    rcont.setParameter(axisname, parameteraddress2, parametervalue2, szStrings2);
    std::cout << "Set address: " << *parameteraddress2 << "\tvalue: " << *parametervalue2 << std::endl;

    //Parameter 12: Closed-Loop Deceleration
    unsigned int *get_parameteraddress2 = new unsigned int(12);
    double *get_parametervalue2 = new double(0);
    char get_szStrings2[2048] = "";
    rcont.getParameter(axisname, get_parameteraddress2, get_parametervalue2, get_szStrings2, 2048);
    std::cout << "Got address: " << *get_parameteraddress2 << "\tvalue: " << *get_parametervalue2 << "\tstring: " << get_szStrings2 << std::endl;

    bool alreadyReferenced = false;
    if (!rcont.reference(axisname, &alreadyReferenced)) //this will move the stage to its refernce switch/limit.
        return -1;

    if (!rcont.move(axisname, target_position))
        return -1;

    rcont.checkMovingState(axisname, new bool, new double);

    if (0) //will move to a random position, makes only real sense for linear stages as a rotational stage has a limit only after a very high number of rotations
    {
        std::cout << "MinLimit: " << rcont.getMinLimit(axisname) << std::endl;
        std::cout << "MaxLimit: " << rcont.getMaxLimit(axisname) << std::endl;
        double travelMin = rcont.getMinLimit(axisname);
        double travelMax = rcont.getMaxLimit(axisname);

        double range = travelMax - travelMin;

        for (int i = 0; i < 10; i++)
        {
            double target = travelMin + (double(rand()) / RAND_MAX) * range;
            std::cout << "moving to " << target << std::endl;
            if (!rcont.move("1", target))
                return -1;
        }
    }
    return 0;
}
