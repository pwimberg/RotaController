# RotaController

This is a library for efficient controlling of a PI (Physik Instrumente) controller inside C++ on Linux.
It was developed for a rotational stage in the CERN CMS Chromie beam telescope by Peter Wimberger (@topsrek) under supervision of Stefano Mersi in Summer 2018, but should be adaptable for (multiple) linear stages.

It depends on the libpi_pi_gsc2 library, PI supplies to make the GCS commands available in C++.

This project is provided as-is with no future development planned.

It is provided under the MIT license (see below).

## Installation

This guide assumes that you have a folder PI_C663 (or similar) which contains, an `INSTALL` script and several folders (For LabView, GCS lookup tables, pi_terminal, drivers etc.)

* Run the PI_C663/INSTALL script with sudo rights, which installs all necessary files in /usr/local/PI. (You may need to run a `chmod +rx -R *` inside PI_C663 beforehand)

* We need the serial number of the device to persistently and reliably mount the controller to the same device name.
  * You need to edit your udev-rules to bind the Controller to the same /dev/name everytime it is connected.
  * Sticking mostly to this tutorial: http://hintshop.ludvig.co.nz/show/persistent-names-usb-serial-devices/
  * Check out `lsusb`, `dmesg` if you don"t know it already.
  * The udev-rules set rules for the kernel to probe/mount/bind a new device to a specific location.
  * As many USB/RS232 devices today use similar chips, we do not want to rely on discriminating them by their VID:PID (Vendor and Product ID).
  * Instead we will use the devices' serial numbers to correctly find them(, bring them all) and bind them. 
  * This can be done by running i.e. `udevadm info -a -n /dev/ttyUSB0 | grep '{serial}' | head -n1`, if it was mounted to /dev/ttyUSB0
  * This command will output something like: `ATTRS{serial}=="1234ABC!"`


* Edit udev rules:
  * `cd /etc/udev/rules.d`
  * `sudo mv 99_pi_ftdi_usb.rules 99_pi_ftdi_usb.rules.old` (doing a backup)
  * `sudo nano 99_pi_ftdi_usb.rules` and enter:
  * `SUBSYSTEM=="tty", ATTRS{idVendor}=="1a72", ATTRS{idProduct}=="1007", ATTRS{serial}=="YOURSERIALNUMBER", SYMLINK+="RotaController"`
  * Ctrl+X, Y, Enter
  * `sudo udevadm control --reload` (reload udevrules manager)
  * replug

* Add your user to group dialout: `sudo usermod -a -G dialout dutuser`
  * Please be aware that, if the /dev/RotaController now does not have correct permissions, it will cause all sort of errors inside RotaController and pi_terminal.

* Clone/Download this repo to your machine and run `make`.

## Usage

Just adapt the RotaControllerTest.cc to your needs, `make` and run it (`./RotaController.exe`) or use the library in your own projects. It is intended to sepcify idfferent logging-streams if needed or to adapt the Error and Logging functions.

If you want to give the controller a command quickly, you can also send it manually with the `pi_terminal` found in `/usr/local/PI/bin`. Or use MikroMove under windows.

## Troubleshoot & Recommendations

Check if every cable is connected in the right way.
Check again.
Check PID settings in the controller (Default settings can be found in the manual).
If the axisname does not exist, you have to find it out by listing all axes and assigning the new name to the old one (`checkListAllAxes()` & `setNewAxisName()`).
Check comments in the code for more help.

It is possible that you can rearrange the reference switch on your stage too. We had to rotate the blue ring on top of our stage (with a 0.9mm allen key and 3 screws)

## License

Copyright 2018 CERN

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

